(function (root, factory) {
    if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else {
        root.ENVS = factory();
    }
})(typeof globalThis !== 'undefined' ? globalThis : typeof self !== 'undefined' ? self : this, function () {
    return Object.freeze({
        envVersion: '4.0.12',
        envType: 'os',
        ssoUrl: 'http://10.30.30.78:38080/sso/',
        deployEnv: 'local',
        osType: 'iscos',
        osVersion: '3.0.0',
        osBase: '3.0.0',
        appVersion: '0.0.0'
    });
});
