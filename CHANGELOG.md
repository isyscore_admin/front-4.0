# 更新日志
### [1.1.1](http://10.30.30.3/fe-group/isc-config-center-ui/compare/v1.1.0...v1.1.1) (2022-11-18)


### 🐛 问题修复

* 去除老版本isc-table-operator、media-ui解决打包问题 ([2239f84](http://10.30.30.3/fe-group/isc-config-center-ui/commit/2239f8416927fedf6a86562213bc50ed46c9f9cd)) by zhumx
* 去除老版本isc-table-operator、media-ui解决打包问题 ([294abe9](http://10.30.30.3/fe-group/isc-config-center-ui/commit/294abe9d1c17bf4d5cc77260aae08a57e979159f)) by zhumx
* 去除老版本isc-table-operator、media-ui解决打包问题 ([5c75fb0](http://10.30.30.3/fe-group/isc-config-center-ui/commit/5c75fb00dad5034feb072793712973f67f9472ea)) by zhumx
* 去除老版本isc-table-operator、media-ui解决打包问题 ([66d2249](http://10.30.30.3/fe-group/isc-config-center-ui/commit/66d2249b539d451df51a811520e1556ef83e5de1)) by zhumx
* 去除老版本isc-table-operator、media-ui解决打包问题 ([7d9345a](http://10.30.30.3/fe-group/isc-config-center-ui/commit/7d9345ac7358fc17e2cb1deff8ecb412e2ba15f3)) by zhumx

## [1.1.0](http://10.30.30.3/fe-group/isc-config-center-ui/compare/v1.0.0...v1.1.0) (2022-08-04)


### 🎉 新增特性

* 未经授权功能实现 ([8f209b7](http://10.30.30.3/fe-group/isc-config-center-ui/commit/8f209b7f19171c5bd0d4e17050f65068b0301943)) by 祝明祥

## 1.0.0 (2022-07-15)


### 🎉 新增特性

* 配置中心项目搭建 ([bbd9ccb](http://10.30.30.3/fe-group/isc-config-center-ui/commit/bbd9ccba80471cbfdba52a5135430b5c4bcedde2)) by 祝明祥
