'use strict';

const http = require('http');

const server = http.createServer((req, res) => {
    res.writeHead(200, {
        'set-cookie': 'yunlai=' + Date.now()
    });
    res.end(JSON.stringify(req.headers));
});

server.listen(2021);
