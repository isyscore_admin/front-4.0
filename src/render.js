import Vue from 'vue';
import App from './App.vue';
import router from './router/index';
import store from './store/index';
import './plugins/base-ui';
import './plugins/admin-ui';
import './plugins/media-ui';
import './plugins/icon-font';
import './plugins/svg-icon';
import './styles/common.scss';
import './styles/base-ui.scss';
import './styles/width.scss';
import '@isyscore/styles';
import * as filters from '@/filter';
import '@/assets/iconfont';
import IscSvgIcon from '@isyscore/icons';
import routes from './router/routes';
import { app } from '@isyscore/messenger';
// import mediaUI from '@isyscore/media-ui';
// Vue.use(mediaUI);
import Clipboard from 'v-clipboard';
Vue.use(Clipboard);
Vue.use(IscSvgIcon, { common: true, nav: true, business: true, mobile: true });

Object.keys(filters).forEach((key) => Vue.filter(key, filters[key]));

Vue.config.productionTip = false;

(async () => {
    new Vue({
        router,
        store,
        render: (h) => h(App)
    }).$mount('#app');
})();
