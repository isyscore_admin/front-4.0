import axios from '@/utils/axiosV2';

export async function getServiceTree() {
    let res = await axios.get('/api/core/platform/meta/queryAllApp2/default');
    return handleServiceTree(res);
}

function handleServiceTree(arr) {
    let ret = arr
        .filter((item) => item.services && item.services.length)
        .map((item) => {
            return {
                label: item.appName,
                value: item.appCode,
                children: item.services.map((service) => {
                    return {
                        value: service.serviceId,
                        label: service.serviceName
                    };
                })
            };
        });
    return ret;
}

export async function getLogServiceTree() {
    let res = await axios.get('/api/core/platform/meta/log/queryAllApp/default');
    return handleServiceTree(res);
}
