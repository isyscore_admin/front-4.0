/**
 * example
 * @author ydr.me
 * @create 2019-09-26 19:48:41
 * @update 2019-09-26 19:48:41
 */

'use strict';

import vue from 'vue';
/**
 * 获取字典管理列表
 * @returns {Promise}
 */

export async function dictList(params) {
    const res = await vue.prototype.$service.get('/kvconfig/api/common/kvconfig/dict/page', { params: params });

    return res.data;
}

/**
 * 增加字典配置
 * @returns {Promise}
 */
export async function addDict(params) {
    const res = await vue.prototype.$service.post('/kvconfig/api/common/kvconfig/dict', params);
    return res.data;
}

/**
 * 修改字典配置
 * @returns {Promise}
 */
export async function editDict(params) {
    const res = await vue.prototype.$service.put('/kvconfig/api/common/kvconfig/dict', params);
    return res.data;
}

/**
 * 删除字典配置
 * @returns {Promise}
 */
export async function deleteDict(params) {
    const res = await vue.prototype.$service.delete('/kvconfig/api/common/kvconfig/dict', {
        headers: { application: 'x-www-form-urlencoded' },
        params: params
    });
    return res.data;
}

/**
 * 获取字典数据列表
 * @returns {Promise}
 */
export async function dictDataList(params) {
    const res = await vue.prototype.$service.get('/kvconfig/api/common/kvconfig/dict/data/page', { params: params });
    return res.data;
}
/**
 * 增加字典数据
 * @returns {Promise}
 */
export async function addDictData(params) {
    const res = await vue.prototype.$service.post('/kvconfig/api/common/kvconfig/dict/data', params);
    return res.data;
}

/**
 * 修改字典数据
 * @returns {Promise}
 */
export async function editDictData(params) {
    const res = await vue.prototype.$service.put('/kvconfig/api/common/kvconfig/dict/data', params);
    return res.data;
}

/**
 * 删除字典数据
 * @returns {Promise}
 */
export async function deleteDictData(params) {
    const res = await vue.prototype.$service.delete('/kvconfig/api/common/kvconfig/dict/data', {
        headers: { application: 'x-www-form-urlencoded' },
        params: params
    });
    return res.data;
}
