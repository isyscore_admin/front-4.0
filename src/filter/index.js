const num = (val) => {
    if (val) return /(\d+(\.\d+)?)/.exec(val)[0];
};

const unit = (val) => {
    if (val) return val.replace(/[^a-zA-Z]/g, '');
};

const runFilter = (val) => {
    return val === 2 ? '运行中' : '已停止';
};

const guardFilter = (val) => {
    if (typeof val === 'undefined') {
        return '';
    } else {
        return val ? '守护中' : '未守护';
    }
};

const workTimeFilter = (val) => {
    if (val) return val.split('天')[0];
};

export { num, unit, runFilter, guardFilter, workTimeFilter };
