// https://vuex.vuejs.org/en/actions.html
import axios from '@/utils/axiosRemote';
export default {
    async getOperationState({ state }) {
        try {
            const data = await axios.get('/api/rc-application/application/search', {
                params: { code: 'operation-center' }
            });
            // console.log(data);
            if (data.records && data.records.length > 0 && data.records[0].status === 1) {
                state.isHasOperation = true;
            } else {
                state.isHasOperation = false;
            }
        } catch (e) {
            console.log(e);
        }
    }
};
