// https://vuex.vuejs.org/en/state.html

export default {
    installer: {
        showDialog: false,
        showNoti: false,
        showDialogClose: false,
        fileName: '',
        showProgress: false,
        uploadPercent: 0,
        speed: 0,
        msg: '',
        success: false,
        error: false,
        activeName: 'package-list',
        packageNum: 0,
        node: '',
        start: 1,
        dbStart: 1,
        serviceStart: 1
    },
    profiles: [],
    isAppActive: true,
    currentTenantId: 'system',
    tenantOptions: [],
    canVisit: true,
    isHasOperation: false
};
