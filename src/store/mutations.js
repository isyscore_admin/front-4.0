// https://vuex.vuejs.org/en/mutations.html

export default {
    //czz 安装进度对话框和toast需全局显示
    setInstallerShowDialog(state, value) {
        state.installer.showDialog = value;
    },
    setInstallerShowDialogClose(state, value) {
        state.installer.showDialogClose = value;
    },
    setInstallerShowProgress(state, value) {
        state.installer.showProgress = value;
    },
    setInstallerUploadPercent(state, value) {
        state.installer.uploadPercent = value;
    },
    setInstallerSpeed(state, value) {
        state.installer.speed = value;
    },
    setInstallerMsg(state, value) {
        state.installer.msg = value;
    },
    setInstallerActiveName(state, value) {
        state.installer.activeName = value;
    },
    setInstallerPackageNum(state, value) {
        state.installer.packageNum = value;
    },
    setInstallerFileName(state, value) {
        state.installer.fileName = value;
    },
    setInstallerSuccess(state, value) {
        state.installer.success = value;
    },
    setInstallerError(state, value) {
        state.installer.error = value;
    },
    setInstallerShowNoti(state, value) {
        state.installer.showNoti = value;
    },
    setInstallerNode(state, value) {
        state.installer.node = value;
    },
    setInstallerStart(state, value) {
        state.installer.start = value;
    },
    setInstallerDBStart(state, value) {
        state.installer.dbStart = value;
    },
    setInstallerServiceStart(state, value) {
        state.installer.serviceStart = value;
    },
    setProfiles(state, value) {
        state.profiles = value;
    },
    setIsAppActive(state, flag) {
        state.isAppActive = flag;
    },
    setCurrentTenantId(state, val) {
        state.currentTenantId = val;
    },
    setTenantOptions(state, val) {
        state.tenantOptions = val;
    },
    setCanVisit(state, val) {
        state.canVisit = val;
    }
};
