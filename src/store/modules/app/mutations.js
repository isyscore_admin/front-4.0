import { set } from '@/utils/vuex';

export default {
    setSnackbar: set('snackbar'),
    setUser: set('userInfo')
};
