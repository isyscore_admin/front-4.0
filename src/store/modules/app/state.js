export default {
    snackbar: {
        color: 'info',
        show: false,
        text: 'This is info snackbar.'
    },
    appList: [],
    userInfo: {}
};
