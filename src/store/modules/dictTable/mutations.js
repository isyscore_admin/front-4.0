export default {
    records(state, records) {
        state.records = records;
    },
    total(state, total) {
        state.total = total;
    },
    current(state, current) {
        state.current = current;
    },
    size(state, size) {
        state.size = size;
    },
    params(state, params) {
        state.params = params;
    }
};
