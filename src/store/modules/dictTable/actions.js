import { dictList } from '../../../services/dictionary';
export default {
    async getDictList({ commit }, params) {
        const res = await dictList(params);
        let { records, total, current, size } = res;
        commit('records', records);
        commit('total', total);
        commit('current', current);
        commit('size', size);
    }
};
