import { dictDataList } from '../../../services/dictionary';
export default {
    async getDictDataList({ commit }, params) {
        const res = await dictDataList(params);
        let { records, total, current, size } = res;
        commit('records', records);
        commit('total', total);
        commit('current', current);
        commit('size', size);
    }
};
