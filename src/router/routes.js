export default [
    {
        path: '/',
        name: '演示模块',
        component: () => import('@/views/Index/index.vue'),
        meta: { menushow: true, icon: 'iscon-rule-engine', title: '演示模块' },
        // redirect: { name: '' },
        children: [

        ]
    },
    {
        path: '*',
        redirect: '/'
    }
];
