/**
 * Vue Router
 *
 * @library
 *
 * https://router.vuejs.org/en/
 */

// Lib imports
import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';
import { app } from '@isyscore/messenger';
import axios from '@/utils/axios';
// Routes
import routes from './routes';
import store from '@/store/index';
Vue.use(Router);

//获取原型对象上的push函数
const originalPush = Router.prototype.push;
//修改原型对象中的push方法
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch((err) => err);
};

// Create a new router
const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routes
});

app.injectRouter(router);

// app.collectUserVisitAction(router);

Vue.use(Meta);

export default router;
