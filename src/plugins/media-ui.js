// import Vue from 'vue';
// import {
//     MediaJsonEditor,
//     MediaLogPrinter,
//     MediaJsonViewer,
//     MediaMarkdownEditor,
//     MediaCodeEditor
// } from '@isyscore/media-ui';

// Vue.use(MediaJsonEditor);
// Vue.use(MediaLogPrinter);
// Vue.use(MediaJsonViewer);
// Vue.use(MediaMarkdownEditor);
// Vue.use(MediaCodeEditor);
import Vue from 'vue'
import JsonViewer from 'vue-json-viewer'

// Import JsonViewer as a Vue.js plugin
Vue.use(JsonViewer)