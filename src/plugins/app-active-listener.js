import store from '../store/index';
window.addEventListener('message', handleActiveChange, false);

function handleActiveChange(e) {
    if (!e) return;
    const statusText = e.data;
    if (!statusText) return;
    if (typeof statusText !== 'string') return;
    if (!statusText.startsWith('isc-desktop-app')) return;
    try {
        const status = statusText.split(':')[1];
        if (status === 'active') {
            store.commit('setIsAppActive', true);
            // window.location.reload();
        } else if (status === 'deactive') {
            store.commit('setIsAppActive', false);
            // let end = setInterval(function () {}, 10000);
            // for (let i = 1; i <= end; i++) {
            //     clearInterval(i);
            // }
        }
    } catch (e) {
        // ignore
    }
}
