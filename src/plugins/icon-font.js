'use strict';

import Vue from 'vue';

import IconFont from '../components/common/icon-font';
Vue.component('IconFont', IconFont);
