const colorList = [
    '#56A64B',
    '#F2CC0D',
    '#E03044',
    '#3274D9',
    '#FF7809',
    '#A352CC',

    '#18730F',
    '#CD9E02',
    '#AD0217',
    '#1250B0',
    '#E55501',
    '#7C2EA4',

    '#37872D',
    '#E0B402',
    '#C4162B',
    '#1F60C5',
    '#FA6402',
    '#8F3BB8',

    '#73BF6A',
    '#FADE2B',
    '#F34A5C',
    '#5795F2',
    '#FF9930',
    '#B877DA',

    '#97D98E',
    '#FFEE52',
    '#FF7384',
    '#8AB9FF',
    '#FFB358',
    '#CA95E5'
];

export default colorList;
