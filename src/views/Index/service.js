import axios from '@/utils/axiosV2';
//创建角色
export const addRole = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/creatRole', { params: params });
};
//创建用户
export const addUser = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/creatUser', { params: params });
};
//用户登录
export const login = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/login', { params: params });
};
//获取用户信息
export const getUserInfo = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/getUserInfo', { params: params });
};
//获取功能权限菜单
export const getAclList = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/getAclList', { params: params });
};
//创建设备类
export const addProduct = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/addProduct', { params: params });
};
//创建设备
export const creatDevice = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/creatDevice', { params: params });
};
//修改设备属性
export const setAttributeValue = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/setAttributeValue', { params: params });
};
//获取设备详情
export const getDeviceDetail = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/getDeviceDetail', { params: params });
};
//查询设备属性
export const getAttributeValue = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/getAttributeValue', { params: params });
};
//生成异常信息
export const createErrorForDemo = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/createErrorForDemo', { params: params });
};
//获取当前动态配置项
export const getDynamicConfig = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/getDynamicConfig', { params: params });
};
//纯前端应用注册
export const registerFrontApp = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/registerFrontApp', { params: params });
};
//纯后端应用注册
export const registerBackEndApp = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/registerBackEndApp', { params: params });
};
//桌面菜单应用注册
export const registerMenuApp = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/registerMenuApp', { params: params });
};

//后台应用注册
export const registerBackgroundApp = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/registerBackgroundApp', { params: params });
};

//外联应用注册
export const registerOutApp = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/registerOutApp', { params: params });
};


//轻应用注册
export const registerLightApp = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/registerLightApp', { params: params });
};

//应用卸载
export const deleteApp = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/deleteApp', { params: params });
};

//注册应用公私钥
export const registerAppKey = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/registerAppKey', { params: params });
};

//获取accessToken
export const getAccessToken = async (params) => {
    return axios.get('/api/app/basic-demo-xyz123/demo/getAccessToken', { params: params });
};
