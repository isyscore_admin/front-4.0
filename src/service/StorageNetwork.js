import axios from '../utils/axios';
import { stringify } from '@isyscore/utils/date';
import { fileSize, fileRate, addTime } from '@/utils/unit';
import store from '@/store/index.js';

const BASE_URL = '/api/ops/monitoring/host';

export const queryDiskInstant = async (id) => {
    try {
        let arr = [0, 0];
        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/disk/instant`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 15 * start }
        });

        res.data.forEach((ele) => {
            if (ele.name === 'total_bytes') arr[0] = fileSize(parseFloat(ele.value).toFixed(2));
            if (ele.name === 'usage_percent') arr[1] = parseFloat(ele.value).toFixed(2);
        });

        return arr;
    } catch (e) {
        return false;
    }
};

export const queryDiskHistorical = async (id) => {
    try {
        let xData = [];
        let yUsedBytes = [];
        let yFreeBytes = [];
        let yReadData = [];
        let yWrittenData = [];
        let yTotalBytesPer = [];
        let yUsedBytesPer = [];
        let yTotalBytesPerMountpoint = [];
        let yUsedBytesPerMountpoint = [];

        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/disk/historical`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 15 * start }
        });

        res.data.forEach((ele) => {
            if (ele.name === 'used_bytes') {
                xData = ele.pairs.map((item) => {
                    return start <= 8
                        ? stringify(item[0] * 1000, 'HH:mm:ss')
                        : stringify(item[0] * 1000, 'MM-DD HH:mm:ss');
                });
                ele.pairs.forEach((item) => {
                    yUsedBytes.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'free_bytes') {
                ele.pairs.forEach((item) => {
                    yFreeBytes.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'read_bytes_rate' && Object.getOwnPropertyNames(ele.groupPairs).length != 0) {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yReadData = groupPairs;
            }
            if (ele.name === 'written_bytes_rate' && Object.getOwnPropertyNames(ele.groupPairs).length != 0) {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yWrittenData = groupPairs;
            }
            if (ele.name === 'total_bytes_per_disk_partition') {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yTotalBytesPer = groupPairs;
            }
            if (ele.name === 'used_bytes_per_disk_partition') {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yUsedBytesPer = groupPairs;
            }

            if (ele.name === 'total_bytes_per_mountpoint' && Object.getOwnPropertyNames(ele.groupPairs).length != 0) {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yTotalBytesPerMountpoint = groupPairs;
            }
            if (ele.name === 'used_bytes_per_mountpoint' && Object.getOwnPropertyNames(ele.groupPairs).length != 0) {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yUsedBytesPerMountpoint = groupPairs;
            }
        });
        return {
            xData,
            yUsedBytes,
            yFreeBytes,
            yReadData,
            yWrittenData,
            yTotalBytesPer,
            yUsedBytesPer,
            yTotalBytesPerMountpoint,
            yUsedBytesPerMountpoint
        };
    } catch (e) {
        return false;
    }
};

export const queryNetworkInstant = async (id) => {
    try {
        let arr = [];
        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/network/instant`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 15 * start }
        });

        for (let key in res.data) {
            let obj = {};
            obj.name = key;
            res.data[key].forEach((ele, i) => {
                if (ele.name === 'transmit_bytes_rate')
                    obj.transmitBytesRate = fileRate(parseFloat(ele.value).toFixed(2));
                if (ele.name === 'receive_bytes_rate')
                    obj.receiveBytesRate = fileRate(parseFloat(ele.value).toFixed(2));
                if (ele.name === 'transmit_packets_rate') obj.transmitPackets = parseFloat(ele.value).toFixed(2);
                if (ele.name === 'receive_packets_rate') obj.treceivePackets = parseFloat(ele.value).toFixed(2);
                if (ele.name === 'transmit_err_packets_rate') obj.transmitErrPackets = parseFloat(ele.value).toFixed(2);
                if (ele.name === 'receive_err_packets_rate') obj.receiveErrPackets = parseFloat(ele.value).toFixed(2);
            });
            arr.push(obj);
        }

        return arr;
    } catch (e) {
        return false;
    }
};

export const queryNetworkHistorical = async (id) => {
    try {
        let xNetworkData = [];
        let yTransmitPackets = [];
        let yReceiveErrPackets = [];
        let yTransmitErrPackets = [];
        let yReceivePackets = [];
        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/network/historical`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 15 * start }
        });

        res.data.forEach((ele) => {
            if (ele.name === 'transmit_packets_rate' && Object.getOwnPropertyNames(ele.groupPairs).length != 0) {
                const groupPairs = ele.groupPairs;
                xNetworkData = Object.values(groupPairs)[0].map((item) => {
                    return start <= 8
                        ? stringify(item[0] * 1000, 'HH:mm:ss')
                        : stringify(item[0] * 1000, 'MM-DD HH:mm:ss');
                });
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yTransmitPackets = groupPairs;
            }
            if (ele.name === 'receive_err_packets_rate') {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yReceiveErrPackets = groupPairs;
            }
            if (ele.name === 'transmit_err_packets_rate') {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yTransmitErrPackets = groupPairs;
            }
            if (ele.name === 'receive_packets_rate') {
                const groupPairs = ele.groupPairs;
                for (let key in groupPairs) {
                    groupPairs[key].forEach((item1, i) => {
                        const arr = item1.map((item2) => {
                            return item2 === null ? null : parseFloat(item2).toFixed(2);
                        });
                        groupPairs[key][i] = arr;
                    });
                }
                yReceivePackets = groupPairs;
            }
        });
        return { xNetworkData, yTransmitPackets, yReceiveErrPackets, yTransmitErrPackets, yReceivePackets };
    } catch (e) {
        return false;
    }
};
