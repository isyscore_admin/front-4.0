import axios from '../utils/axios';
import { stringify } from '@isyscore/utils/date';
import { fileSize, addTime } from '@/utils/unit';
import store from '@/store/index.js';

const BASE_URL = '/api/ops/monitoring/host';

export const queryCpuInstant = async (id) => {
    try {
        let arr = [0, 0, 0, 0, 0];
        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/cpu/instant`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 60 * start }
        });

        res.data.forEach((ele) => {
            if (ele.name === 'idle_percent') arr[0] = parseFloat(ele.value).toFixed(2);
            if (ele.name === 'io_wait_percent') arr[1] = parseFloat(ele.value).toFixed(2);
            if (ele.name === 'system_usage_percent') arr[2] = parseFloat(ele.value).toFixed(2);
            if (ele.name === 'user_usage_percent') arr[3] = parseFloat(ele.value).toFixed(2);
            if (ele.name === 'cpu_number') arr[4] = parseInt(ele.value);
        });

        return arr;
    } catch (e) {
        return false;
    }
};

export const queryCpuHistorical = async (id) => {
    try {
        let xTotalData = [];
        let yTotalData = [];
        let yload1Data = [];
        let yload5Data = [];
        let yload15Data = [];
        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/cpu/historical`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 15 * start }
        });

        res.data.forEach((ele) => {
            if (ele.name === 'total_usage_percent' && ele.pairs.length > 0) {
                xTotalData = ele.pairs.map((item) => {
                    return start <= 8
                        ? stringify(item[0] * 1000, 'HH:mm:ss')
                        : stringify(item[0] * 1000, 'MM-DD HH:mm:ss');
                });
                ele.pairs.forEach((item) => {
                    yTotalData.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'load_1m' && ele.pairs.length > 0) {
                ele.pairs.forEach((item) => {
                    yload1Data.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'load_5m' && ele.pairs.length > 0) {
                ele.pairs.forEach((item) => {
                    yload5Data.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'load_15m' && ele.pairs.length > 0) {
                ele.pairs.forEach((item) => {
                    yload15Data.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
        });
        return { xTotalData, yTotalData, yload1Data, yload5Data, yload15Data };
    } catch (e) {
        return false;
    }
};

export const queryMemInstant = async (id) => {
    try {
        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/mem/instant`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 15 * start }
        });
        let arr0 = fileSize(Number(res.data[0].value).toFixed(2));
        let arr1 = fileSize(Number(res.data[1].value).toFixed(2));
        let arr2 = Number(Number(res.data[4].value).toFixed(2));
        let arr3 = Number((Number(res.data[3].value) - Number(res.data[4].value)).toFixed(2));
        let arr4 = Number((100 - arr2 - arr3).toFixed(2));
        return [arr0, arr1, arr2, arr3, arr4];
    } catch (e) {
        return false;
    }
};

export const queryMemHistorical = async (id) => {
    try {
        let xData = [];
        let yFreeData = [];
        let yUsedData = [];
        let yBuffersData = [];
        let yCachedData = [];
        let yUsedUtilizationData = [];
        let start = store.state.installer.start;
        let nowStart = parseInt((new Date().getTime() - start * 3600000) / 1000);
        const res = await axios.get(`${BASE_URL}/mem/historical`, {
            headers: { node: store.state.installer.node, start: nowStart, step: 15 * start }
        });

        res.data.forEach((ele) => {
            if (ele.name === 'free_bytes' && ele.pairs.length > 0) {
                xData = ele.pairs.map((item) => {
                    return start <= 8
                        ? stringify(item[0] * 1000, 'HH:mm:ss')
                        : stringify(item[0] * 1000, 'MM-DD HH:mm:ss');
                });
                ele.pairs.forEach((item) => {
                    yFreeData.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'actual_used_bytes' && ele.pairs.length > 0) {
                ele.pairs.forEach((item) => {
                    yUsedData.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'buffers_bytes' && ele.pairs.length > 0) {
                ele.pairs.forEach((item) => {
                    yBuffersData.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'cached_bytes' && ele.pairs.length > 0) {
                ele.pairs.forEach((item) => {
                    yCachedData.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
            if (ele.name === 'actual_used_utilization_percent' && ele.pairs.length > 0) {
                ele.pairs.forEach((item) => {
                    yUsedUtilizationData.push(item[1] === null ? null : parseFloat(item[1]).toFixed(2));
                });
            }
        });
        return { xData, yFreeData, yUsedData, yBuffersData, yCachedData, yUsedUtilizationData };
    } catch (e) {
        return false;
    }
};
