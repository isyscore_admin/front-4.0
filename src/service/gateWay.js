import axios from '../utils/axios';

const BASE_URL = '/api/device/twin-link/gateway';

export const queryList = async (id) => {
    try {
        const res = await axios.get(`${BASE_URL}/list`);

        res.data.map((ele) => {
            ele.children = ele.driverCaseStatusVOList;
            ele.gatewayId = ele.id;
        });
        res.data.forEach((item1) => {
            const gatewayId = item1.gatewayId;
            item1.children.forEach((item2) => {
                item2.gatewayId = gatewayId;
                item2.driverCaseId = item2.id;
            });
        });
        return res.data;
    } catch (e) {
        return false;
    }
};

export const controlGateway = async (params) => {
    try {
        const res = await axios.post(`${BASE_URL}/control`, params);

        return res.data;
    } catch (e) {
        return false;
    }
};
