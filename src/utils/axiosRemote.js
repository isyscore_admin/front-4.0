import { app } from '@isyscore/messenger';
import Axios, { apiVersion, userToken, processFault, errorTrace } from '@isyscore/axios';
import store from '@/store';
import { error } from '@/utils/message';
const axios = Axios.create();
const apiVersion2Feature = apiVersion({
    version: '2.1'
});
const userTokenFeature = userToken({
    headerKey: 'token',
    // tokenVal() {
    //     return store.state.sso ? store.state.sso.token : '';
    // }
    tokenVal() {
        return app.getUserToken() || store.state.sso ? store.state.sso.token : '';
    }
});

const processFaultFeature = processFault({
    appMessenger: app,
    processor(fault) {
        const { code, message } = fault;
        // if (code === 'license.invalid' || message === 'license.invalid') {
        //     app.licenseInvalid();
        //     error(message);
        //     return;
        // }
        if (message) {
            // error(message);
        } else {
            error('网络异常，请稍后重试～～');
        }
    }
});
axios.features
    .use(apiVersion2Feature)
    .use(userTokenFeature)
    .use(processFaultFeature)
    .use(errorTrace({ appMessenger: app }));

export default axios;
