'use strict';

export const delay = (timeout = 1000) => {
    return new Promise((resolve) => {
        setTimeout(resolve, timeout);
    });
};

export const each = async (list, iterator) => {
    for (let i = 0; i < list.length; i++) {
        await iterator(list[i], i);
    }
};
