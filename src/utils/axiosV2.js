import { app } from '@isyscore/messenger';
import Axios, { apiVersion, userToken, processFault, errorTrace } from '@isyscore/axios';
import router from '@/router';
import store from '@/store';
import { error, warning } from '@/utils/message';
const axios = Axios.create();
const apiVersion2Feature = apiVersion({
    version: '2.1'
});
const userTokenFeature = userToken({
    headerKey: 'token',
    tokenVal() {
        return app.getUserToken();
    }
});

const processFaultFeature = processFault({
    appMessenger: app,
    processor(fault) {
        const { message, status, code } = fault;
        if (message) {
            if (code === 1010401) {
                warning(message);
            } else {
                error(message);
            }
        } else {
            error('网络异常，请稍后重试～～');
        }
        if (status === 401) {
            location.href = `${ENVS.ssoUrl}?return=${encodeURIComponent(location.href)}`;
        }
    }
});
axios.interceptors.response.use(
    response=>{
        // console.log(response);
        response.data.data.traceid = response.headers['t-head-traceid'] || '';
        return response;
    },error => {
        // 响应失败
        return Promise.reject(error);
    }
);
axios.features.use(apiVersion2Feature).use(userTokenFeature).use(processFaultFeature).use(errorTrace({ appMessenger: app }));
axios.interceptors.request.use(
    config => {
        // console.log(config);
        return config;
    },error => {
        // 请求异常
        return Promise.reject(error);
    }
);
export default axios;
