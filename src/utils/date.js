export function addZero(num) {
    if (typeof num !== 'number') return num;
    return num >= 10 ? num.toString() : '0' + num.toString();
}

export function addThreeDays(num) {
    return new Date(num + 3 * 24 * 3600 * 1000).getTime();
}
