'use strict';

import { abbr } from '@isyscore/utils/number';

export const fileSize = (value) => {
    const units = [' B', ' KB', ' MB', ' GB', ' TB'];
    return abbr(value, units, 1024, 2);
};

export const netSize = (value) => {
    const units = [' bps', ' kbps', ' mbps', ' gbps', ' tbps'];
    return abbr(value, units, 1024, 2);
};

export const fileRate = (value) => {
    const units = [' B/s', ' KB/s', ' MB/s', ' GB/s', ' TB/s'];
    return abbr(value, units, 1024, 2);
};

export const addTime = (start, end, step, arr) => {
    if (arr.length === 0) {
        arr.push(end * 1000);
    }
    if (end - start > step) {
        arr.push((end - step) * 1000);
        return addTime(start, end - step, step, arr);
    } else {
        return arr.reverse();
    }
};
