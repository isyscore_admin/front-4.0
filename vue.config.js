// vue.config.js
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const TestWebpackPlugin = require('./src/plugins/test-webpack-plugin');
const TestPrefetchPlugin = require('./src/plugins/test-prefetch-plugin');

module.exports = {
    publicPath: '/app/basic-demo-xyz123',
    devServer: {
        proxy: {
            // '/ws/': {
            //     ws: true,
            //     target: 'http://10.30.30.20:38080',
            //     logLevel: 'debug'
            // },
            // '/api/ops/monitoring/alert/rule/': {
            //     target: 'http://10.30.30.78:35100',
            //     logLevel: 'debug'
            // },
            // '/api/ops/monitoring/': {
            //     target: 'http://192.168.8.115:35100',
            //     logLevel: 'debug'
            // },
            '/api/': {
                target: 'http://192.168.10.107:8857',
                // target: 'http://192.168.10.248:8888',
                logLevel: 'debug'
            }
        }
    },
    lintOnSave: false,
    configureWebpack: {
        plugins: [new TestPrefetchPlugin(), new TestWebpackPlugin()]
    },

    chainWebpack: (config) => {
        config.plugin('monaco').use(new MonacoWebpackPlugin());
        config.module.rule('svg-sprite').use('svgo-loader').loader('svgo-loader');

        config.optimization.minimizer('terser').tap((args) => {
            args[0].terserOptions.compress.drop_debugger = true;
            args[0].terserOptions.compress.drop_console = true;
            return args;
        });
    },

    pluginOptions: {
        svgSprite: {
            dir: 'src/assets/svg',
            test: /\.(svg)(\?.*)?$/,
            loaderOptions: {
                extract: true,
                spriteFilename: 'img/icons.[hash:8].svg'
            },
            pluginOptions: {
                plainSprite: true
            }
        }
    },
    transpileDependencies: [/@isyscore/i, 'vue-clamp', 'vue-echarts', 'resize-detector']
};
