/**
 * prettier.config.js
 * @ref https://prettier.io/
 */

module.exports = {
    printWidth: 120,
    tabWidth: 4,
    useTabs: false,
    singleQuote: true,
    semi: true,
    htmlWhitespaceSensitivity: 'ignore'
};
