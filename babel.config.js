module.exports = {
    presets: ['@vue/cli-plugin-babel/preset'],

    plugins: [
        [
            'component',
            {
                libraryName: '@isyscore/base-ui',
                styleLibraryName: 'theme-chalk'
            },
            '@isyscore/base-ui'
        ]
        // [
        //     'component',
        //     {
        //         libraryName: '@isyscore/admin-ui',
        //         styleLibraryName: 'theme-chalk'
        //     },
        //     '@isyscore/admin-ui'
        // ]
    ]
};
